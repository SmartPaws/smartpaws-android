package org.smartpaws;

import android.app.Activity;
import android.os.Bundle;

import org.smartpaws.fragments.PreferenceFragment;


public class PreferenceActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new PreferenceFragment()).commit();
    }
}
