package org.smartpaws.objects;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

public class Convention implements Serializable {

    private String name;
    private String venue;

    @SerializedName("date_start")
    private Date dateStart;

    @SerializedName("days")
    private int duration;

    @SerializedName("twitter")
    private String[] twitterAccounts;

    private String theme;
    private Event[] events;
    private Room[] rooms;
    private GuestOfHonor[] gohs;
    private User[] users;
    private int[] dealers;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String[] getTwitterAccounts() {
        return twitterAccounts;
    }

    public void setTwitterAccounts(String[] twitterAccounts) {
        this.twitterAccounts = twitterAccounts;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public Event[] getEvents() {
        for (Event e : events) {
            e.setConvention(this);
        }
        return events;
    }

    public void setEvents(Event[] events) {
        for (Event e : events) {
            e.setConvention(this);
        }
        this.events = events;
    }

    public Room[] getRooms() {
        return rooms;
    }

    public void setRooms(Room[] rooms) {
        this.rooms = rooms;
    }

    public GuestOfHonor[] getGuestsOfHonor() {
        return gohs;
    }

    public void setGuestsOfHonor(GuestOfHonor[] gohs) {
        this.gohs = gohs;
    }

    public User[] getUsers() {
        return users;
    }

    public User[] getUsers(int[] userIds) {
        User[] result = new User[userIds.length];
        for (int i=0; i<result.length && i<userIds.length; i++) {
            for (User u : this.users) {
                if (u.getId() == userIds[i]) {
                    result[i] = u;
                }
            }
        }
        return result;
    }

    public void setUsers(User[] users) {
        this.users = users;
    }

    public void setDealers(int[] dealers) {
        this.dealers = dealers;
    }

    public User[] getDealers() {
        User[] result = new User[dealers.length];
        User[] users = getUsers();
        for (User u : users) {
            int index = Arrays.binarySearch(dealers, u.getId());
            if (index > -1) {
                result[index] = u;
            }
        }
        return result;
    }

    protected Room getRoom(int room) {
        for (Room r : rooms) {
            if (r.getId() == room) return r;
        }
        return null;
    }
}
