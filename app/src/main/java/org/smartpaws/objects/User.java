package org.smartpaws.objects;

import java.io.Serializable;

public class User implements Serializable {

    private int id;
    private String name;
    private String description;
    private String table;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description != null ? this.description : "No description available.";
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }
}
