package org.smartpaws.objects.twitter;

import com.google.gson.annotations.SerializedName;

import org.smartpaws.objects.twitter.url.TwitterUrl;

import java.io.Serializable;
import java.util.Date;

public class Tweet implements Serializable {

    @SerializedName("created_at")
    private Date createdAt;

    private String text;
    private TwitterUser user;
    private TwitterEntity entities;

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public TwitterUser getUser() {
        return user;
    }

    public void setUser(TwitterUser user) {
        this.user = user;
    }

    public TwitterEntity getEntities() {
        return entities;
    }

    public void setEntities(TwitterEntity entities) {
        this.entities = entities;
    }

    public TwitterUrl[] getUrls() {
        return entities.getUrls();
    }

    public void getUrls(TwitterUrl[] urls) {
        this.entities.setUrls(urls);
    }
}
