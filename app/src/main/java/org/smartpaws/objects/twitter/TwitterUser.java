package org.smartpaws.objects.twitter;

import com.google.gson.annotations.SerializedName;

public class TwitterUser {

    private String name;

    @SerializedName("screen_name")
    private String screenName;

    @SerializedName("profile_image_url_https")
    private String profileImageUrl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }
}
