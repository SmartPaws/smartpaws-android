package org.smartpaws.objects.twitter;

import org.smartpaws.objects.twitter.url.TwitterUrl;
import org.smartpaws.objects.twitter.url.TwitterUrlContainer;

public class TwitterEntity {

    private TwitterUrlContainer url;

    public TwitterUrl[] getUrls() {
        return url.getUrls();
    }

    public void setUrls(TwitterUrl[] urls) {
        this.url.setUrls(urls);
    }

    public TwitterUrlContainer getUrlContainer() {
        return url;
    }

    public void setUrlContainer(TwitterUrlContainer url) {
        this.url = url;
    }
}
