package org.smartpaws.objects.twitter.url;

import com.google.gson.annotations.SerializedName;

public class TwitterUrl {

    @SerializedName("expanded_url")
    private String url;

    @SerializedName("display_url")
    private String displayUrl;
}
