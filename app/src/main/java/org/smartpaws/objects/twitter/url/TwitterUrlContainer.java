package org.smartpaws.objects.twitter.url;

public class TwitterUrlContainer {

    private TwitterUrl[] urls;

    public TwitterUrl[] getUrls() {
        return urls;
    }

    public void setUrls(TwitterUrl[] urls) {
        this.urls = urls;
    }
}
