package org.smartpaws.objects;

public class Reminder {

    private int eventid;
    private int advance;

    public Reminder(int eventid, int advance) { this.eventid = eventid; this.advance = advance; }

    public int getEventId() {
        return eventid;
    }

    public void setEventId(int eventid) {
        this.eventid = eventid;
    }

    public int getAdvance() {
        return advance;
    }

    public void setAdvance(int advance) {
        this.advance = advance;
    }

}
