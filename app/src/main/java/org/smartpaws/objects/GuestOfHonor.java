package org.smartpaws.objects;

import org.smartpaws.util.CountryCode;

import java.io.Serializable;

public class GuestOfHonor implements Serializable {

    private int id;
    private String country;
    private String name;
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CountryCode getCountryCode() {
        return CountryCode.fromName(country);
    }

    public void setCountryCode(CountryCode code) {
        this.country = code.name();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
