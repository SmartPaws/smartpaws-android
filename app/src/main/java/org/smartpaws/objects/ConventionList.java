package org.smartpaws.objects;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class ConventionList {

    private String[] conventions;

    public String[] asStringArray() {
        return conventions;
    }

    public Set<String> asStringSet() {
        Set<String> result = new HashSet<>();
        for (String c : conventions) result.add(c);
        return Collections.unmodifiableSet(result);
    }
}
