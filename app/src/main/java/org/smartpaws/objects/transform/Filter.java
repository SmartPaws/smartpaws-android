package org.smartpaws.objects.transform;

import org.smartpaws.objects.Event;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public final class Filter {

    private static final int MORNING_TIME = 6;

    private Filter() {}

    public static Event[] byDate(Event[] events, Date date) {
        List<Event> result = new ArrayList<Event>();
        Calendar dateCal = Calendar.getInstance();
        dateCal.setTime(date);
        dateCal.set(Calendar.HOUR_OF_DAY, 0);
        dateCal.set(Calendar.MINUTE, 0);
        dateCal.set(Calendar.SECOND, 0);
        dateCal.set(Calendar.MILLISECOND, 1);

        Calendar eventCal = Calendar.getInstance();
        for (int i=0; i<events.length && i<events.length; i++) {
            eventCal.setTime(events[i].getDateStart());

            if ((eventCal.get(Calendar.DAY_OF_MONTH) == dateCal.get(Calendar.DAY_OF_MONTH)
                    && eventCal.get(Calendar.HOUR_OF_DAY) > MORNING_TIME)
                    || (dateCal.get(Calendar.DAY_OF_MONTH) == eventCal.get(Calendar.DAY_OF_MONTH) - 1
                    && eventCal.get(Calendar.HOUR_OF_DAY) < MORNING_TIME)) {

                result.add(events[i]);
            }
        }
        return result.toArray(new Event[result.size()]);
    }
}
