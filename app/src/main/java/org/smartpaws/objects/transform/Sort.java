package org.smartpaws.objects.transform;

import org.smartpaws.objects.Convention;
import org.smartpaws.objects.Event;
import org.smartpaws.objects.Room;
import org.smartpaws.objects.User;

import java.util.Arrays;
import java.util.Comparator;

public final class Sort {

    private Sort() {}

    public static void byDate(Convention[] conventions) {
        Arrays.sort(conventions, new Comparator<Convention>() {
            @Override
            public int compare(Convention lhs, Convention rhs) {
                return lhs.getDateStart().compareTo(rhs.getDateStart());
            }
        });
    }

    public static void byDate(Event[] events) {
        Arrays.sort(events, new Comparator<Event>() {
            @Override
            public int compare(Event lhs, Event rhs) {
                return lhs.getDateStart().compareTo(rhs.getDateStart());
            }
        });
    }

    public static void byDuration(Event[] events) {
        Arrays.sort(events, new Comparator<Event>() {
            @Override
            public int compare(Event lhs, Event rhs) {
                return lhs.getDuration() < rhs.getDuration() ? -1 : (lhs.getDuration() == rhs.getDuration() ? 0 : 1);
            }
        });
    }

    public static void byName(Event[] events) {
        Arrays.sort(events, new Comparator<Event>() {
            @Override
            public int compare(Event lhs, Event rhs) {
                return lhs.getName().compareTo(rhs.getName());
            }
        });
    }

    public static void byName(Room[] rooms) {
        Arrays.sort(rooms, new Comparator<Room>() {
            @Override
            public int compare(Room lhs, Room rhs) {
                return lhs.getName().compareTo(rhs.getName());
            }
        });
    }

    public static void byName(User[] users) {
        Arrays.sort(users, new Comparator<User>() {
            @Override
            public int compare(User lhs, User rhs) {
                return lhs.getName().compareTo(rhs.getName());
            }
        });
    }

    public static void byId(Room[] rooms) {
        Arrays.sort(rooms, new Comparator<Room>() {
            @Override
            public int compare(Room lhs, Room rhs) {
                return lhs.getId() < rhs.getId() ? -1 : (lhs.getId() == rhs.getId() ? 0 : 1);
            }
        });
    }

    public static void byId(User[] users) {
        Arrays.sort(users, new Comparator<User>() {
            @Override
            public int compare(User lhs, User rhs) {
                return lhs.getId() < rhs.getId() ? -1 : (lhs.getId() == rhs.getId() ? 0 : 1);
            }
        });
    }

    public static void byRoom(Event[] events) {
        Arrays.sort(events, new Comparator<Event>() {
            @Override
            public int compare(Event lhs, Event rhs) {
                return lhs.getRoom().getName().compareTo(rhs.getRoom().getName());
            }
        });
    }
}
