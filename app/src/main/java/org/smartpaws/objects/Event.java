package org.smartpaws.objects;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class Event implements Serializable {

    private transient Convention convention;
    private int id;
    private String name;

    @SerializedName("date_start")
    private Date dateStart;

    private int duration;
    private int room;
    private String description;
    private int[] users;

    protected void setConvention(Convention convention) {
        this.convention = convention;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getRoomId() {
        return room;
    }

    public void setRoomId(int room) {
        this.room = room;
    }

    public Room getRoom() {
        return convention.getRoom(room);
    }

    public void setRoom(Room room) {
        this.room = room.getId();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User[] getUsers() {
        return convention.getUsers(users);
    }

    public void setUsers(User[] users) {
        int[] result = new int[users.length];
        for (int i=0; i<result.length && i<users.length; i++) {
            result[i] = users[i].getId();
        }
        this.users = result;
    }
}
