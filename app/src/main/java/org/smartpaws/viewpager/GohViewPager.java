package org.smartpaws.viewpager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.smartpaws.MainActivity;
import org.smartpaws.R;
import org.smartpaws.fragments.GohFragment;
import org.smartpaws.net.DataMan;
import org.smartpaws.net.ParallelResponseHandler;
import org.smartpaws.net.ResponseHandler;
import org.smartpaws.objects.Convention;
import org.smartpaws.objects.GuestOfHonor;
import org.smartpaws.util.ScreenDensity;

public class GohViewPager extends Fragment {

    private static final int IMAGE_COUNT_PER_FRAGMENT = 2;

    private volatile int fetchedCount;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final int containerId = getArguments().getInt("container_id");
        final View loading = inflater.inflate(R.layout.frag_loading, container, false);

        DataMan.getConvention("confuzzled_staff", new ResponseHandler<Convention>() {
            @Override
            public void result(Convention result) {
                final GuestOfHonor[] gohs = result.getGuestsOfHonor();

                final byte[][] bannerData = new byte[gohs.length][];
                final byte[][] flagData = new byte[gohs.length][];

                fetchedCount = 0;

                for (int i=0; i<gohs.length; i++) {
                    DataMan.getParallelByteArray(i, "img/goh/" + gohs[i].getId() + "/" + ScreenDensity.getName(), ".png", new ParallelResponseHandler<byte[]>() {

                        @Override
                        public void result(int index, byte[] result) {
                            bannerData[index] = result;
                            if (pollAllFetched(gohs.length)) {
                                showFragment(containerId, gohs, bannerData, flagData);
                            }
                        }
                    });

                    DataMan.getParallelByteArray(i, "img/flag/" + gohs[i].getCountryCode() + "/" + ScreenDensity.getName(), ".png", new ParallelResponseHandler<byte[]>() {

                        @Override
                        public void result(int index, byte[] result) {
                            flagData[index] = result;
                            if (pollAllFetched(gohs.length)) {
                                showFragment(containerId, gohs, bannerData, flagData);
                            }
                        }
                    });
                }

            }
        });

        return loading;
    }

    private synchronized boolean pollAllFetched(int gohCount) {
        fetchedCount++;
        return fetchedCount == (IMAGE_COUNT_PER_FRAGMENT * gohCount);
    }

    private void showFragment(int containerId, GuestOfHonor[] gohs, byte[][] gohBanners, byte[][] gohFlags) {
        final Fragment fragment = new GohViewPagerFragment();
        final Bundle bundle = new Bundle();
        bundle.putSerializable("gohs", gohs);

        for (int i=0; i<gohBanners.length; i++) {
            bundle.putSerializable("banners", gohBanners);
            bundle.putSerializable("flags", gohFlags);
        }

        fragment.setArguments(bundle);

        MainActivity.INSTANCE.getSupportFragmentManager()
                .beginTransaction()
                .replace(containerId, fragment)
                .commit();
    }

    public static class GohViewPagerFragment extends Fragment {

        private ViewPager mPager;
        private PagerAdapter mPagerAdapter;
        private GuestOfHonor[] gohs;
        private byte[][] bannerData;
        private byte[][] flagData;

        @Override
        public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            this.gohs = (GuestOfHonor[]) getArguments().getSerializable("gohs");
            this.bannerData = (byte[][]) getArguments().getSerializable("banners");
            this.flagData = (byte[][]) getArguments().getSerializable("flags");

            View root = inflater.inflate(R.layout.view_pager, null);

            mPagerAdapter = new GohViewPagerAdapter(MainActivity.INSTANCE.getSupportFragmentManager());
            mPager = (ViewPager) root.findViewById(R.id.pager);
            mPager.setAdapter(mPagerAdapter);
            return root;
        }

        private GohFragment createFragment(GuestOfHonor goh, byte[] banner, byte[] flag) {
            GohFragment fragment = new GohFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("goh", goh);
            bundle.putByteArray("banner", banner);
            bundle.putByteArray("flag", flag);
            fragment.setArguments(bundle);

            return fragment;
        }

        private class GohViewPagerAdapter extends FragmentStatePagerAdapter {
            public GohViewPagerAdapter(FragmentManager fm) {
                super(fm);
            }

            @Override
            public Fragment getItem(int pos) {
                return createFragment(gohs[pos], bannerData[pos], flagData[pos]);
            }

            @Override
            public int getCount() {
                return gohs.length;
            }
        }
    }
}
