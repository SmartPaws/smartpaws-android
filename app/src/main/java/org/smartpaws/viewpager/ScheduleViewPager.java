package org.smartpaws.viewpager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.smartpaws.MainActivity;
import org.smartpaws.R;
import org.smartpaws.fragments.ScheduleFragment;
import org.smartpaws.net.DataMan;
import org.smartpaws.net.ResponseHandler;
import org.smartpaws.objects.Convention;

public class ScheduleViewPager extends Fragment {

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final int containerId = getArguments().getInt("container_id");
        final View loading = inflater.inflate(R.layout.frag_loading, null);

        DataMan.getConvention("confuzzled_staff", new ResponseHandler<Convention>() {
            @Override
            public void result(Convention result) {

                Fragment fragment = new ScheduleViewPagerFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("convention", result);
                fragment.setArguments(bundle);

                MainActivity.INSTANCE.getSupportFragmentManager()
                        .beginTransaction()
                        .replace(containerId, fragment)
                        .commit();
            }
        });

        return loading;
    }

    public static class ScheduleViewPagerFragment extends Fragment {

        private ViewPager mPager;
        private PagerAdapter mPagerAdapter;
        private Convention convention;

        @Override
        public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            this.convention = (Convention) getArguments().getSerializable("convention");

            View root = inflater.inflate(R.layout.view_pager, null);

            mPagerAdapter = new ScheduleViewPagerAdapter(MainActivity.INSTANCE.getSupportFragmentManager());
            mPager = (ViewPager) root.findViewById(R.id.pager);
            mPager.setAdapter(mPagerAdapter);
            return root;
        }

        private ScheduleFragment createFragment(Convention convention, int day) {
            ScheduleFragment fragment = new ScheduleFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("convention", convention);
            bundle.putInt("day", day);
            fragment.setArguments(bundle);

            return fragment;
        }

        private class ScheduleViewPagerAdapter extends FragmentStatePagerAdapter {
            public ScheduleViewPagerAdapter(FragmentManager fm) {
                super(fm);
            }

            @Override
            public Fragment getItem(int position) {
                return createFragment(convention, position);
            }

            @Override
            public int getCount() {
                return convention.getDuration();
            }
        }
    }
}
