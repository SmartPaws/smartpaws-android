package org.smartpaws.notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;

import static org.smartpaws.MainActivity.INSTANCE;
import org.smartpaws.R;
import org.smartpaws.net.DataManager;
import org.smartpaws.objects.Event;

import java.util.Date;
import java.util.Locale;

public class AlarmReceiver extends BroadcastReceiver {

    public static int INDEX = 0;

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationManager notificationManager;
        notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

        Event event = DataManager.GSON.fromJson(intent.getStringExtra("event"), Event.class);
        String room = intent.getStringExtra("event_room");
        Notification notification = notifyEvent(context, event, room);

        notificationManager.notify(INDEX, notification);
    }

    private static Notification notifyEvent(Context context, Event event, String room) {
        Intent intent = new Intent(context, NotificationReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, INDEX, intent, 0);

        String timeTill = getTimeTill(event.getName(), event.getDateStart());

        return new NotificationCompat.Builder(context)
                .setContentTitle(event.getName())
                .setContentText(timeTill + " - " + room)
                //.setSmallIcon(R.drawable.ic_notification)
                .setColor(Color.argb(255, 255, 143, 0))
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setLights(Color.argb(255, 174, 234, 0), 1000, 500)
                .setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND)
                .build();
    }

    private static String getTimeTill(String eventName, Date date) {
        int secondsTill = Math.round((date.getTime() - System.currentTimeMillis()) / 1000);

        if (secondsTill < 0) return "the distant past";
        if (secondsTill < 60) {
            if (secondsTill == 1) {
                return INSTANCE.getString(R.string.notif_event_start_sec, eventName);
            }
            return INSTANCE.getString(R.string.notif_event_start_secs, eventName, secondsTill);
        }
        if (secondsTill >= 60) {
            int minsTill = Math.round(secondsTill / 60);
            if (minsTill == 1) return INSTANCE.getString(R.string.notif_event_start_min, eventName);
            if (minsTill >= 60) {
                long hoursTill = Math.round(minsTill / 60);
                if (hoursTill == 1) return INSTANCE.getString(R.string.notif_event_start_hour, eventName);
                return INSTANCE.getString(R.string.notif_event_start_hours, eventName, hoursTill);
            }
            return INSTANCE.getString(R.string.notif_event_start_mins, eventName, minsTill);
        }
        return INSTANCE.getString(R.string.notif_event_start_soon, eventName);
    }
}
