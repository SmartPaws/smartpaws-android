package org.smartpaws.notifications;

import android.app.Activity;
import android.os.Bundle;

import org.smartpaws.R;

public class NotificationReceiver extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_receiver);
    }
}
