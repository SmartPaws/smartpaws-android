package org.smartpaws.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AlarmBootRefresh extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        AlarmService.reloadAlarms();
    }

}
