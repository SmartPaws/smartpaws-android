package org.smartpaws.notifications;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.smartpaws.MainActivity;
import org.smartpaws.net.DataManager;
import org.smartpaws.objects.Event;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

public class AlarmService {
    private static Context context;
    private static ConcurrentHashMap<Integer, Integer> reminders;

    public static final Gson GSON = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .setPrettyPrinting().create();

    public AlarmService(Context con) {
        context = con;
        reminders = getReminders();
    }

    public static int getReminderForEvent(Event event) {
        if (reminders.containsKey(event.getId()))
        {
            return reminders.get(event.getId());
        } else {
            return 0;
        }
    }

    public static void startAlarm(Event event, int minsBefore) {
        //if (event.getDateStart().before(new Date(System.currentTimeMillis()))) return;
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra("event", DataManager.GSON.toJson(event));
        intent.putExtra("event_room", event.getRoom().getName());
        PendingIntent alarmSender = PendingIntent.getBroadcast(context, ++AlarmReceiver.INDEX, intent, 0);

        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, event.getDateStart().getTime() - (minsBefore * 60 * 1000), alarmSender);

        reminders.put(event.getId(), minsBefore);
        saveReminders();
    }

    public static void removeAlarm(Event event) {
        Intent intent = new Intent(context, AlarmReceiver.class);
        PendingIntent alarmSender = PendingIntent.getBroadcast(context, 0, intent, 0); //I don't think the request code is checked, could be wrong.

        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(alarmSender);

        reminders.remove(event.getId());
        saveReminders();
    }

    public static void reloadAlarms() {

        Event[] e = DataManager.getInstance().getSelectedConvention().getEvents();

        for (Event n : e)
        {
            if (reminders.containsKey(n.getId()))
            {
                if (n.getDateStart().before(new Date(System.currentTimeMillis()))) return;
                Intent intent = new Intent(context, AlarmReceiver.class);
                intent.putExtra("event", DataManager.GSON.toJson(n));
                intent.putExtra("event_room", n.getRoom().getName());
                PendingIntent alarmSender = PendingIntent.getBroadcast(context, ++AlarmReceiver.INDEX, intent, 0);

                AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
                alarmManager.set(AlarmManager.RTC_WAKEUP, n.getDateStart().getTime() - (reminders.get(n.getId()) * 60 * 1000), alarmSender);
            }
        }
    }

    private static ConcurrentHashMap getReminders() {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(context.openFileInput("reminders.json"), "UTF-8"));
            String line;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();
            return GSON.fromJson(sb.toString(), new TypeToken<ConcurrentHashMap<Integer, Integer>>(){}.getType());
        } catch (FileNotFoundException ex) { // expected behaviour - file will be created on save.
            return new ConcurrentHashMap<Integer, Integer>();
        } catch (IOException ex) {
            Log.e(MainActivity.APP_NAME, "FileIO Exception:", ex);
            return null;
        }
    }

    private static void saveReminders() {
        try {
        FileOutputStream o = context.openFileOutput("reminders.json", Context.MODE_PRIVATE);
        o.write(GSON.toJson(reminders).getBytes());
        o.close();
        } catch (FileNotFoundException ex) { //Why this needs to be here I don't know.
            Log.e(MainActivity.APP_NAME, "FileNotFound Exception:", ex);
        } catch (IOException ex) {
            Log.e(MainActivity.APP_NAME, "FileIO Exception:", ex);
        }
    }


}
