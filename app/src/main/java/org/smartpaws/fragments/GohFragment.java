package org.smartpaws.fragments;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.smartpaws.R;
import org.smartpaws.objects.GuestOfHonor;

public class GohFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        GuestOfHonor goh = (GuestOfHonor) getArguments().getSerializable("goh");
        byte[] bannerData = getArguments().getByteArray("banner");
        byte[] flagData = getArguments().getByteArray("flag");

        final View root = inflater.inflate(R.layout.frag_goh, null);

        ((ImageView) root.findViewById(R.id.card_goh_image)).setImageBitmap(
                BitmapFactory.decodeByteArray(bannerData, 0, bannerData.length)
        );

        ((ImageView) root.findViewById(R.id.card_goh_location_flag)).setImageBitmap(
                BitmapFactory.decodeByteArray(flagData, 0, flagData.length)
        );

        ((TextView)root.findViewById(R.id.card_goh_name)).setText(goh.getName());
        ((TextView)root.findViewById(R.id.card_goh_location_name)).setText(goh.getCountryCode().getCountry());
        ((TextView)root.findViewById(R.id.card_goh_description)).setText(Html.fromHtml(goh.getDescription()));
        return root;
    }
}
