package org.smartpaws.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.smartpaws.MainActivity;
import org.smartpaws.R;
import org.smartpaws.net.DataMan;
import org.smartpaws.net.DataManager;
import org.smartpaws.net.ResponseHandler;
import org.smartpaws.objects.Convention;
import org.smartpaws.objects.twitter.Tweet;
import org.smartpaws.recycler.TwitterRecyclerView;
import org.smartpaws.util.DividerItemDecoration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TwitterFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final int containerId = getArguments().getInt("container_id");
        final View loading = inflater.inflate(R.layout.frag_loading, null);

        DataMan.getConvention("confuzzled_staff", new ResponseHandler<Convention>() {
            @Override
            public void result(Convention result) {

                Fragment fragment = new TwitterContentFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("convention", result);
                fragment.setArguments(bundle);

                MainActivity.INSTANCE.getSupportFragmentManager()
                        .beginTransaction()
                        .replace(containerId, fragment)
                        .commit();
            }
        });

        return loading;
    }

    public static class TwitterContentFragment extends Fragment {
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            Convention convention = (Convention) getArguments().getSerializable("convention");

            final View root = inflater.inflate(R.layout.frag_twitter, null);
            final RecyclerView recycler = (RecyclerView) root.findViewById(R.id.twitterList);

            final List<Tweet> tweets = new ArrayList<>();
            String[] accounts = convention.getTwitterAccounts();
            for (int i=0; i<accounts.length; i++) {
                DataManager.getInstance().downloadTweets(accounts[i], new ResponseHandler<Tweet[]>() {
                    @Override
                    public void result(Tweet[] result) {
                        Collections.addAll(tweets, result);
                        recycler.setAdapter(new TwitterRecyclerView(root.getContext(), tweets.toArray(new Tweet[tweets.size()])));
                        recycler.invalidate();
                    }
                });
            }

            recycler.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
            recycler.setHasFixedSize(true);
            LinearLayoutManager llm = new LinearLayoutManager(root.getContext());
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            recycler.setLayoutManager(llm);

            return root;
        }
    }
}
