package org.smartpaws.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.smartpaws.R;
import org.smartpaws.recycler.HomeRecyclerView;

public class HomeFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_home, null);

        RecyclerView recycler = (RecyclerView) root.findViewById(R.id.card_home);
        recycler.setAdapter(new HomeRecyclerView());
        recycler.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(root.getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(llm);

        return root;
    }
}
