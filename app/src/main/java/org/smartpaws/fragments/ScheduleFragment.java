package org.smartpaws.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.smartpaws.R;
import org.smartpaws.objects.Convention;
import org.smartpaws.objects.Event;
import org.smartpaws.objects.transform.Filter;
import org.smartpaws.recycler.ScheduleRecyclerView;
import org.smartpaws.util.TimeUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public class ScheduleFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Convention convention = (Convention) getArguments().getSerializable("convention");
        int day = getArguments().getInt("day");

        // TODO: Fix date filters, disabled for now

        Calendar cal = Calendar.getInstance();
        cal.setTime(convention.getDateStart());
        cal.add(Calendar.DATE, day);
        Event[] events = Filter.byDate(convention.getEvents(), cal.getTime());


        View root = inflater.inflate(R.layout.frag_schedule, null);

        TextView dateView = (TextView) root.findViewById(R.id.schedule_date);

        SimpleDateFormat formatStart = new SimpleDateFormat("EEEE, dd", Locale.getDefault());
        SimpleDateFormat formatEnd = new SimpleDateFormat(" MMMM yyyy", Locale.getDefault());

        formatStart.setTimeZone(TimeZone.getDefault());
        formatEnd.setTimeZone(TimeZone.getDefault());

        dateView.setText(formatStart.format(cal.getTime())
                + TimeUtils.getDayOfMonthSuffix(cal.get(Calendar.DAY_OF_MONTH))
                + formatEnd.format(cal.getTime()));

        RecyclerView recycler = (RecyclerView) root.findViewById(R.id.cardList);
        recycler.setAdapter(new ScheduleRecyclerView(events));
        recycler.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(root.getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(llm);

        return root;
    }

}
