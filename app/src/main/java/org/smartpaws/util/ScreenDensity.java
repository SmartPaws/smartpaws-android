package org.smartpaws.util;

import org.smartpaws.MainActivity;

public final class ScreenDensity {

    @Deprecated
    public static String append(String fileName) {
        int pos = fileName.lastIndexOf('.');
        if (pos >= 0) {
            String extension = fileName.substring(pos);
            fileName = fileName.substring(0, pos);

            fileName = "img/" + fileName + "/" + getName() + extension;
        }
        return fileName;
    }

    public static String getName() {
        int density = (int) MainActivity.INSTANCE.getResources().getDisplayMetrics().density * 100;

        String result;
        switch (density) {
            case 75: {
                result = "ldpi";
                break;
            }
            case 100: {
                result = "mdpi";
                break;
            }
            case 150: {
                result = "hdpi";
                break;
            }
            case 200: {
                result = "xhdpi";
                break;
            }
            case 213: {
                result = "tvdpi";
                break;
            }
            case 300: {
                result = "xxhdpi";
                break;
            }
            case 400: {
                result = "xxxhdpi";
                break;
            }
            default: {
                result = "mdpi";
                break;
            }
        }
        return result;
    }
}
