package org.smartpaws.util;

import java.util.Date;

public final class TimeUtils {

    private static final int SECOND = 1;
    private static final int MINUTE = 60 * SECOND;
    private static final int HOUR = 60 * MINUTE;
    private static final int DAY = 24 * HOUR;
    private static final int MONTH = 30 * DAY;

    public static String getDayOfMonthSuffix(final int n) {
        if (n < 1 || n > 31) return "";

        if (n >= 11 && n <= 13) {
            return "th";
        }

        switch (n % 10) {
            case 1:  return "st";
            case 2:  return "nd";
            case 3:  return "rd";
            default: return "th";
        }
    }

    public static String toFuzzyTime(Date date) {

        TimeSpan ts = new TimeSpan(System.currentTimeMillis() - date.getTime());
        double delta = Math.abs(ts.getSeconds());

        if (delta < 0)
        {
            return "Ergh, I don't know when.";
        }
        if (delta < 1 * MINUTE)
        {
            return ts.getSeconds() == 1 ? "a second ago" : ts.getSeconds() + " seconds ago";
        }
        if (delta < 2 * MINUTE)
        {
            return "a minute ago";
        }
        if (delta < 45 * MINUTE)
        {
            return ts.getMinutes() + " minutes ago";
        }
        if (delta < 90 * MINUTE)
        {
            return "an hour ago";
        }
        if (delta < 24 * HOUR)
        {
            return ts.getHours() + " hours ago";
        }
        if (delta < 48 * HOUR)
        {
            return "yesterday";
        }
        if (delta < 30 * DAY)
        {
            return ts.getDays() + " days ago";
        }
        if (delta < 12 * MONTH)
        {
            int months = (int) Math.floor((double)ts.getDays() / 30);
            return months <= 1 ? "a month ago" : months + " months ago";
        }
        else
        {
            int years = (int) Math.floor((double)ts.getDays() / 365);
            return years <= 1 ? "one year ago" : years + " years ago";
        }
    }

    private static class TimeSpan {

        private final long span;

        public TimeSpan(long spanMillis) {
            this.span = spanMillis;
        }

        public long getSeconds() {
            return Math.round(span / 1000);
        }

        public long getMinutes() {
            return Math.round((span / 1000) / 60);
        }

        public int getHours() {
            return Math.round(((span / 1000) / 60) / 60);
        }

        public int getDays() {
            return Math.round((((span / 1000) / 60) / 60) / 24);
        }
    }

}
