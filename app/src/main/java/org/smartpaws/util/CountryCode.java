package org.smartpaws.util;

import org.smartpaws.MainActivity;
import org.smartpaws.R;

/**
 * Country codes in ISO 3166-1 alpha-2 format
 */
public enum CountryCode {

    AU("Australia"),
    BE("Belgium"),
    BR("Brazil"),
    DE("Germany"),
    FI("Finland"),
    FR("France"),
    GB(MainActivity.INSTANCE.getString(R.string.country_gb)),
    GR("Greece"),
    IO("British Indian Ocean Territory"),
    IT("Italy"),
    JP("Japan"),
    NL(MainActivity.INSTANCE.getString(R.string.country_nl)),
    KZ("Kazakhstan"),
    NZ("New Zealand"),
    RU("Russia"),
    SE("Sweden"),
    SG("Singapore"),
    US("United States"),
    ZA("South Africa");

    private final String country;

    CountryCode(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public static CountryCode fromName(String country) {
        for (CountryCode code : values()) {
            if (code.name().equalsIgnoreCase(country)) return code;
        }
        return US;
    }

    public static CountryCode fromCountryName(String country) {
        for (CountryCode code : values()) {
            if (code.getCountry().equalsIgnoreCase(country)) return code;
        }
        return US;
    }
}
