package org.smartpaws.recycler;

import android.content.Context;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import org.smartpaws.R;
import org.smartpaws.net.DataManager;
import org.smartpaws.net.ResponseHandler;
import org.smartpaws.objects.User;
import org.smartpaws.recycler.cards.CardDealer;

public class DealerRecyclerView extends RecyclerView.Adapter<CardDealer> {

    private Context context;
    private final User[] dealers;

    public DealerRecyclerView(Context context, User[] dealers) {
        this.context = context;
        this.dealers = dealers;
    }

    @Override
    public CardDealer onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new CardDealer(
                LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.card_dealer, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(final CardDealer holder, int i) {
        User user = dealers[i];
        if (user != null) holder.setUser(user);

        DataManager.getInstance().downloadImage(
                PreferenceManager.getDefaultSharedPreferences(context).getString("smartpaws.selected_convention", "confuzzled").toLowerCase()
                        + "/dealer/" + user.getId() + ".png", false, new ResponseHandler<Bitmap>() {
                    @Override
                    public void result(Bitmap result) {
                        holder.setIcon(result);
                    }
                });
    }

    @Override
    public int getItemCount() {
        return dealers.length;
    }
}
