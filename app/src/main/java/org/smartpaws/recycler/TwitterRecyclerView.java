package org.smartpaws.recycler;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import org.smartpaws.R;
import org.smartpaws.net.DataManager;
import org.smartpaws.net.ResponseHandler;
import org.smartpaws.objects.twitter.Tweet;
import org.smartpaws.recycler.cards.CardTwitter;

public class TwitterRecyclerView extends RecyclerView.Adapter<CardTwitter> {

    private Context context;
    private final Tweet[] tweets;

    public TwitterRecyclerView(Context context, Tweet[] tweets) {
        this.context = context;
        this.tweets = tweets;
    }

    @Override
    public CardTwitter onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new CardTwitter(
                LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_tweet, viewGroup, false), tweets[i]);
    }

    @Override
    public void onBindViewHolder(final CardTwitter holder, int i) {
        Tweet tweet = tweets[i];

        holder.setName("@" + tweet.getUser().getScreenName());
        holder.setContent(tweet.getText());
        holder.setDate(tweet.getCreatedAt());
        DataManager.getInstance().downloadImage(tweet.getUser().getProfileImageUrl(), true, new ResponseHandler<Bitmap>() {
            @Override
            public void result(Bitmap result) {
                holder.setIcon(result);
            }
        });
    }

    @Override
    public int getItemCount() {
        return tweets.length;
    }
}
