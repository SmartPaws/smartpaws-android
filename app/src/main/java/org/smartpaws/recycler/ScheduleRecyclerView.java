package org.smartpaws.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import org.smartpaws.R;
import org.smartpaws.notifications.AlarmService;
import org.smartpaws.objects.Event;
import org.smartpaws.objects.transform.Sort;
import org.smartpaws.recycler.cards.CardEvent;

public class ScheduleRecyclerView extends RecyclerView.Adapter<CardEvent> {

    private final Event[] events;

    public ScheduleRecyclerView(Event[] events) {
        this.events = events;
        Sort.byDate(this.events);
    }

    @Override
    public CardEvent onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new CardEvent(
                LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_event, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(CardEvent holder, int i) {
        Event event = events[i];
        holder.setTitle(event.getName());
        holder.setDate(event.getDateStart(), event.getDuration());
        holder.setRoom(event.getRoom());
        holder.setDescription(event.getDescription());
        holder.setUsers(event.getUsers());
        holder.setEvent(event);
        holder.setReminder(AlarmService.getReminderForEvent(event));
    }

    @Override
    public int getItemCount() {
        return events.length;
    }
}
