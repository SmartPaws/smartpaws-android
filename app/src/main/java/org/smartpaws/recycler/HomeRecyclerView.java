package org.smartpaws.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import org.smartpaws.R;
import org.smartpaws.recycler.cards.CardHome;

public class HomeRecyclerView extends RecyclerView.Adapter<CardHome> {

    @Override
    public CardHome onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new CardHome(
                LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_home_title, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(CardHome holder, int i) {
    }

    @Override
    public int getItemCount() {
        return 1;
    }
}
