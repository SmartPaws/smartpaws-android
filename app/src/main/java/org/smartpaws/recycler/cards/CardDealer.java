package org.smartpaws.recycler.cards;

import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.smartpaws.R;
import org.smartpaws.objects.User;

public class CardDealer extends RecyclerView.ViewHolder {

    private User user;
    private final View root;
    private final TextView name;
    private final ImageView icon;

    public CardDealer(View v) {
        super(v);

        this.root = v;
        this.name = (TextView) v.findViewById(R.id.card_dealer_name);
        this.icon = (ImageView) v.findViewById(R.id.card_dealer_icon);
    }

    public void setUser(final User user) {
        this.user = user;

        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                new AlertDialog.Builder(v.getContext())
                        .setCancelable(true)
                        .setTitle(user.getName())
                        .setMessage(user.getDescription() + "\n\n" + "Table: " + user.getTable())
                        .setPositiveButton("Done", null)
                        .show();
            }
        });
        this.name.setText(user.getName());
    }

    public void setIcon(Bitmap icon) {
        this.icon.setImageBitmap(icon);
    }
}
