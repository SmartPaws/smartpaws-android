package org.smartpaws.recycler.cards;

import android.animation.LayoutTransition;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.smartpaws.R;
import org.smartpaws.notifications.AlarmService;
import org.smartpaws.objects.Event;
import org.smartpaws.objects.Room;
import org.smartpaws.objects.User;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class CardEvent extends RecyclerView.ViewHolder {

    private static final SimpleDateFormat startFormat = new SimpleDateFormat("EEE HH:mm");
    private static final SimpleDateFormat endFormat = new SimpleDateFormat("HH:mm");

    private final TextView title;
    private final TextView time;
    private final TextView room;

    private final ViewGroup panel;
    private final ViewGroup reminder;
    private final View panelContent;


    private Event event;

    public CardEvent(View v) {
        super(v);
        this.title = (TextView) v.findViewById(R.id.card_event_title);
        this.time = (TextView) v.findViewById(R.id.card_event_time);
        this.room = (TextView) v.findViewById(R.id.card_event_room);
        this.panel = (ViewGroup) v.findViewById(R.id.card_expand);
        this.reminder = (ViewGroup) v.findViewById(R.id.card_reminder);

        // Set SimpleDateFormats to display the date/time in the user's timezone
        startFormat.setTimeZone(TimeZone.getDefault());
        endFormat.setTimeZone(TimeZone.getDefault());

        final LayoutTransition transition = panel.getLayoutTransition();

        this.panelContent = LayoutInflater.from(v.getContext()).inflate(R.layout.card_event_panel, null);

        final ImageView expand = (ImageView) v.findViewById(R.id.card_expand_ic);

        v.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(final View v) {
                final String[] times = new String[]{"5 minutes before", "10 minutes before", "15 minutes before", "30 minutes before", "1 hour before", "2 hours before"};

                boolean a = (reminder.findViewById(R.id.event_reminder_ic).getVisibility() == View.VISIBLE);

                AlertDialog.Builder b = new AlertDialog.Builder(v.getContext());

                        b.setCancelable(true)
                        .setTitle("Set Reminder")
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .setSingleChoiceItems(times, 1, null)
                        .setPositiveButton((a ? "Update" : "Set"), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                int pos = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                                int minsBefore;
                                String minsText = null;
                                switch (pos) {
                                    case 0:
                                        minsBefore = 5;
                                        break;
                                    case 1:
                                        minsBefore = 10;
                                        break;
                                    case 2:
                                        minsBefore = 15;
                                        break;
                                    case 3:
                                        minsBefore = 30;
                                        break;
                                    case 4:
                                        minsBefore = 60;
                                        minsText = "1 hour";
                                        break;
                                    case 5:
                                        minsBefore = 120;
                                        minsText = "2 hours";
                                        break;
                                    default:
                                        minsBefore = 10;
                                        break;
                                }
                                AlarmService.startAlarm(event, minsBefore);
                                Toast.makeText(v.getContext(), "Reminder set for " + (minsText != null ? minsText : minsBefore + " minutes") + " before event", Toast.LENGTH_SHORT).show();
                                setReminderText((minsText != null ? minsText : minsBefore + " mins"));

                            }
                        });
                        if (a)
                        {
                            b.setNeutralButton("Remove",  new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                   AlarmService.removeAlarm(event);
                                   Toast.makeText(v.getContext(), "Reminder removed", Toast.LENGTH_SHORT).show();
                                   clearReminder();
                                }
                            });
                        }
                        b.create().show();
                return true;
            }
        });

        v.setOnClickListener(new View.OnClickListener() {

            boolean open = false;

            @Override
            public void onClick(View v) {
                if (open) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        panel.setLayoutTransition(null);
                    }
                    panel.removeView(panelContent);
                    expand.setImageResource(R.drawable.ic_menu_down_black_36dp);
                    open = false;
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        panel.setLayoutTransition(transition);
                    }
                    panel.addView(panelContent);
                    expand.setImageResource(R.drawable.ic_menu_up_black_36dp);
                    open = true;
                }
            }
        });
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }

    public void setDate(Date date, int duration) {
        StringBuilder result = new StringBuilder(startFormat.format(date));

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, duration);
        result.append(" - ");
        result.append(endFormat.format(cal.getTime()));

        this.time.setText(result);
    }

    public void setRoom(Room room) {
        this.room.setText(room.getName());
    }

    public void setDescription(String description) {
        ((TextView) panelContent.findViewById(R.id.card_event_description)).setText(description);
    }

    public void setUsers(User[] users) {
        StringBuilder b = new StringBuilder();
        for (int i=0; i<users.length; i++) {
            b.append(users[i].getName());
            if (i < users.length-1) b.append(", ");
        }
        ((TextView) panelContent.findViewById(R.id.card_event_users)).setText(b.toString());
    }

    public void setReminder(int minsVal) {
        int minsBefore;
        String minsText = null;
        switch (minsVal) {
            case 5:
                minsBefore = 5;
                break;
            case 10:
                minsBefore = 10;
                break;
            case 15:
                minsBefore = 15;
                break;
            case 30:
                minsBefore = 30;
                break;
            case 60:
                minsBefore = 60;
                minsText = "1 hour";
                break;
            case 120:
                minsBefore = 120;
                minsText = "2 hours";
                break;
            default:
                minsBefore = 0;
                break;
        }
        if (minsBefore != 0) setReminderText((minsText != null ? minsText : minsBefore + " mins"));
    }

    private void setReminderText(String minsText) {
        reminder.findViewById(R.id.event_reminder_ic).setVisibility(View.VISIBLE);
        reminder.findViewById(R.id.event_reminder_time).setVisibility(View.VISIBLE);
        ((TextView) reminder.findViewById(R.id.event_reminder_time)).setText(minsText);
    }

    public void clearReminder() {
        reminder.findViewById(R.id.event_reminder_ic).setVisibility(View.INVISIBLE);
        reminder.findViewById(R.id.event_reminder_time).setVisibility(View.INVISIBLE);
        ((TextView) reminder.findViewById(R.id.event_reminder_time)).setText("");
    }

    private static void expand(final View v) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        v.getLayoutParams().height = 0;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int)(targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    private static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1){
                    v.setVisibility(View.GONE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                }
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

}
