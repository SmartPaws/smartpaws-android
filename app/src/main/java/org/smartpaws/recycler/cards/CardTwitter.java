package org.smartpaws.recycler.cards;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.smartpaws.R;
import org.smartpaws.objects.twitter.Tweet;
import org.smartpaws.util.TimeUtils;

import java.util.Date;

public class CardTwitter extends RecyclerView.ViewHolder {

    private final Tweet tweet;
    private final TextView name;
    private final TextView content;
    private final TextView date;
    private final ImageView icon;

    public CardTwitter(View v, Tweet t) {
        super(v);
        this.tweet = t;

        this.name = (TextView) v.findViewById(R.id.card_tweet_name);
        this.name.setText(tweet.getUser().getName());

        this.content = (TextView) v.findViewById(R.id.card_tweet_content);
        this.content.setText(tweet.getText());

        this.date = (TextView) v.findViewById(R.id.card_tweet_fuzzy_time);
        this.date.setText(TimeUtils.toFuzzyTime(tweet.getCreatedAt()));

        this.icon = (ImageView) v.findViewById(R.id.card_tweet_icon);
    }

    public void setName(String name) {
        this.name.setText(name);
    }

    public void setContent(String content) {
        this.content.setText(Html.fromHtml(content));
    }

    public void setDate(Date date) {
        this.date.setText(TimeUtils.toFuzzyTime(date));
    }

    public void setIcon(Bitmap icon) {
        this.icon.setImageBitmap(icon);
    }
}
