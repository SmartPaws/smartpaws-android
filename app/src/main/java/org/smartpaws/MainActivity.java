package org.smartpaws;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;

import org.smartpaws.fragments.DealerFragment;
import org.smartpaws.fragments.ErrorTbcFragment;
import org.smartpaws.fragments.HomeFragment;
import org.smartpaws.fragments.TwitterFragment;
import org.smartpaws.net.DataMan;
import org.smartpaws.net.DataManager;
import org.smartpaws.net.ResponseHandler;
import org.smartpaws.notifications.AlarmService;
import org.smartpaws.util.ScreenDensity;
import org.smartpaws.viewpager.GohViewPager;
import org.smartpaws.viewpager.ScheduleViewPager;

import java.util.ArrayList;
import java.util.List;

import br.liveo.interfaces.NavigationLiveoListener;
import br.liveo.navigationliveo.NavigationLiveo;

public class MainActivity extends NavigationLiveo implements NavigationLiveoListener {

    public static final String APP_NAME = "org.smartpaws";
    public static MainActivity INSTANCE;

    private final Fragment[] drawerFragments = new Fragment[7];
    private final List<String> mListNameItem = new ArrayList<>();

    private int currentPos;

    @Override
    public void onUserInformation() {
        if (INSTANCE == null) INSTANCE = this;

        this.mUserName.setText("ConFuzzled");
        this.mUserPhoto.setImageResource(R.drawable.logo_confuzzled);
        DataMan.getImage("confuzzled/nav_header/" + ScreenDensity.getName(), ".jpg", new ResponseHandler<Bitmap>() {
            @Override
            public void result(Bitmap result) {
                MainActivity.this.mUserBackground.setImageBitmap(result);
            }
        });
    }

    @Override
    public void onInt(Bundle savedInstanceState) {
        INSTANCE = this;

        this.setNavigationListener(this);
        this.setDefaultStartPositionNavigation(0);

        mListNameItem.add(0, getString(R.string.nav_home));
        drawerFragments[0] = new HomeFragment();

        mListNameItem.add(1, getString(R.string.nav_schedule));
        drawerFragments[1] = new ScheduleViewPager();

        mListNameItem.add(2, getString(R.string.nav_goh));
        drawerFragments[2] = new GohViewPager();

        mListNameItem.add(3, getString(R.string.nav_dealers_den));
        drawerFragments[3] = new DealerFragment();

        mListNameItem.add(4, getString(R.string.nav_twitter));
        drawerFragments[4] = new TwitterFragment();

        mListNameItem.add(5, getString(R.string.nav_maps));
        drawerFragments[5] = new ErrorTbcFragment();

        mListNameItem.add(6, getString(R.string.nav_local_area));
        drawerFragments[6] = new ErrorTbcFragment();

        List<Integer> mListIconItem = new ArrayList<>();
        mListIconItem.add(0, R.drawable.ic_timetable_grey600_24dp);
        mListIconItem.add(1, R.drawable.ic_timetable_grey600_24dp);
        mListIconItem.add(2, R.drawable.ic_star_grey600_24dp);
        mListIconItem.add(3, R.drawable.ic_cart_grey600_24dp);
        mListIconItem.add(4, R.drawable.ic_twitter_grey600_24dp);
        mListIconItem.add(5, R.drawable.ic_map_grey600_24dp);
        mListIconItem.add(6, R.drawable.ic_city_grey600_24dp);
        this.setFooterInformationDrawer(R.string.nav_settings, 0);

        this.setNavigationAdapter(mListNameItem, mListIconItem, null, null);

        new AlarmService(this);
        new DataManager(this, org.smartpaws.net.HttpClient.BASE_URL); // Legacy

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setBackgroundColor(Color.parseColor("#36296D"));
            setSupportActionBar(toolbar);
        }
    }

    @Override
    public void onItemClickNavigation(int pos, int layoutContainerId) {
        if (pos == currentPos) return;

        Bundle bundle = new Bundle();
        bundle.putInt("container_id", layoutContainerId);
        drawerFragments[pos].setArguments(bundle);
        currentPos = pos;

        getSupportFragmentManager().beginTransaction().replace(layoutContainerId, drawerFragments[pos]).commit();
    }

    @Override
    public void onPrepareOptionsMenuNavigation(Menu menu, int i, boolean b) {
    }

    @Override
    public void onClickFooterItemNavigation(View view) {
        Intent intent = new Intent();
        intent.setClass(MainActivity.this, PreferenceActivity.class);
        startActivityForResult(intent, 0);
    }

    @Override
    public void onClickUserPhotoNavigation(View view) {
    }
}
