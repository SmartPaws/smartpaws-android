package org.smartpaws.net;

public interface ParallelResponseHandler<T> {

    public void result(int index, T result);
}
