package org.smartpaws.net;

public interface Postback {

    public void result(byte[] result);
}
