package org.smartpaws.net;

public interface JsonResponseHandler<T> {

    public void result(T result);
}
