package org.smartpaws.net;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.ResponseHandlerInterface;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.smartpaws.MainActivity;
import org.smartpaws.objects.Convention;
import org.smartpaws.objects.twitter.Authenticated;
import org.smartpaws.objects.twitter.Tweet;
import org.smartpaws.util.ScreenDensity;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;

/**
 * @deprecated Superseded by {@link org.smartpaws.net.DataMan}
 */
@Deprecated
public class DataManager {

    public static final Gson GSON = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .setPrettyPrinting().create();

    public static final Gson GSON_TWITTER = new GsonBuilder()
            .setDateFormat("EEE MMM dd HH:mm:ss ZZZZZ yyyy")
            .setPrettyPrinting().create();

    public static final String SP_API_URL = "http://smartpaws.org/app/";
    private static final String TOKEN_URL = "https://api.twitter.com/oauth2/token";
    private static final String STREAM_URL = "https://api.twitter.com/1.1/statuses/user_timeline.json?count=50&exclude_replies=true&screen_name=";
    private static final String CONSUMER_KEY = "w7sTTR0VjHK0r3mANz0yHFk3q";
    private static final String CONSUMER_SECRET = "TUlqMWms1hsSoLjyTuLF0I9gZfVdbBACpjhqLvoFAmnzt6et6V";
    private final SharedPreferences preferences;

    private static DataManager INSTANCE;

    private Activity activity;
    private String dataUri;
    private Convention selectedConvention;

    public DataManager(Activity activity, String dataUri) {
        INSTANCE = this;
        this.activity = activity;
        this.preferences = PreferenceManager.getDefaultSharedPreferences(activity);
        this.dataUri = dataUri;

        /*
         * Pull data from the cache via the HTTP layer
         * This acts as the local file store. Advised not to tamper with.
         */
        enableCache();
    }

    public static DataManager getInstance() {
        return INSTANCE;
    }

    public String getSelectedConventionString() {
        return preferences.getString("smartpaws.selected_convention", "confuzzled");
    }

    public void setSelectedConvention(Convention convention) {
        this.selectedConvention = convention;
    }

    public Convention getSelectedConvention() {
        return selectedConvention;
    }

    /*
     * Enables HTTP caching on all devices above Gingerbread by using reflection
     */
    private void enableCache() {
        try {
            long httpCacheSize = 20 * 1000 * 1000; // 20 MB
            File httpCacheDir = new File(activity.getCacheDir(), "http");
            Class.forName("android.net.http.HttpResponseCache").getMethod("install", File.class, long.class).invoke(null, httpCacheDir, httpCacheSize);
        } catch (Exception ex) {
            Log.i("org.smartpaws", "Could not enable HTTP caching, continuing without it");
        }
    }

    public RequestHandle downloadUrl(final String uri, final Postback postback) {
        return new AsyncHttpClient().get(uri, new Handler(uri, postback));
    }

    public <T> void downloadObject(String suffixUri, final Class<T> objectClass, final ResponseHandler<T> response) {
        download(suffixUri, false, new ByteResponseHandler() {
            @Override
            public void result(byte[] result) {
                try {
                    String jsonString = new String(result, "UTF-8");
                    response.result(GSON.fromJson(jsonString, objectClass));
                } catch (Exception ex) {
                    Log.e(MainActivity.APP_NAME, "DataManager Exception:", ex);
                }
            }
        });
    }

    public void downloadImage(String uri, boolean isFullUrl, final ResponseHandler<Bitmap> response) {
        download((isFullUrl ? uri : ScreenDensity.append(uri)), isFullUrl, new ByteResponseHandler() {
            @Override
            public void result(byte[] bitmap) {
                response.result(BitmapFactory.decodeByteArray(bitmap, 0, bitmap.length));
            }
        });
    }

    private void download(final String suffixUri, boolean isFullUrl, final ByteResponseHandler response) {
        final String uri = (!isFullUrl ? dataUri : "") + suffixUri;

        new AsyncTask<Void, Void, byte[]>() {
            @Override
            protected byte[] doInBackground(Void... params) {
                try {
                    HttpURLConnection connection = (HttpURLConnection) new URL(uri).openConnection();

                    try {
                        InputStream in = new BufferedInputStream(connection.getInputStream());
                        ByteArrayOutputStream out = new ByteArrayOutputStream();
                        int signed;
                        while ((signed = in.read()) != -1) out.write(signed);
                        in.close();
                        out.flush();
                        return out.toByteArray();
                    } finally {
                        connection.disconnect();
                    }
                } catch (Exception ex) {
                    Log.e(MainActivity.APP_NAME, "DataManager Exception:", ex);
                }
                return new byte[0];
            }

            @Override
            protected void onPostExecute(byte[] result) {
                response.result(result);
            }
        }.execute();
    }

    public void downloadTweets(final String user, final ResponseHandler<Tweet[]> response) {
        new AsyncTask<Void, Void, Tweet[]>() {
            @Override
            protected Tweet[] doInBackground(Void... params) {
                try {
                    String base64Encoded = Base64.encodeToString((CONSUMER_KEY + ":" + CONSUMER_SECRET).getBytes(), Base64.NO_WRAP);

                    HttpPost httpPost = new HttpPost(TOKEN_URL);
                    httpPost.setHeader("Authorization", "Basic " + base64Encoded);
                    httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
                    httpPost.setEntity(new StringEntity("grant_type=client_credentials"));

                    byte[] responseBytes = getResponseBody(httpPost);
                    String rawAuthorization = new String(responseBytes, Charset.forName("UTF-8"));
                    Log.e(MainActivity.APP_NAME, rawAuthorization);
                    Authenticated auth = DataManager.GSON_TWITTER.fromJson(rawAuthorization, Authenticated.class);

                    if (auth != null && auth.token_type.equals("bearer")) {
                        HttpGet httpGet = new HttpGet(STREAM_URL + user);
                        httpGet.setHeader("Authorization", "Bearer " + auth.access_token);
                        httpGet.setHeader("Content-Type", "application/json");

                        String tweetsJson = new String(getResponseBody(httpGet), Charset.forName("UTF-8"));
                        return GSON_TWITTER.fromJson(tweetsJson, Tweet[].class);
                    }
                } catch (Exception ex) {
                    Log.e(MainActivity.APP_NAME, "DataManager Exception:", ex);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Tweet[] result) {
                response.result(result);
            }
        }.execute();
    }

    private byte[] getResponseBody(HttpRequestBase post) throws IOException {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response = client.execute(post);
        Log.e(MainActivity.APP_NAME, "Twitter HTTP Status: " + response.getStatusLine().getStatusCode());
        if (response.getStatusLine().getStatusCode() != 200) {
            response.getEntity().getContent().close();
            return null;
        }
        return EntityUtils.toByteArray(response.getEntity());
    }

    public interface ByteResponseHandler {
        void result(byte[] result);
    }

    class Handler implements ResponseHandlerInterface {

        private final String uri;
        private final Postback postback;

        private boolean sync = false;
        private Header[] headers = new Header[0];

        Handler(String uri, Postback postback) {
            this.uri = uri;
            this.postback = postback;
        }

        @Override
        public void sendResponseMessage(HttpResponse response) throws IOException {
        }

        @Override
        public void sendStartMessage() {}

        @Override
        public void sendFinishMessage() {}

        @Override
        public void sendProgressMessage(int bytesWritten, int bytesTotal) {}

        @Override
        public void sendCancelMessage() {}

        @Override
        public void sendSuccessMessage(int statusCode, Header[] headers, byte[] responseBody) {
            postback.result(responseBody);
        }

        @Override
        public void sendFailureMessage(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {}

        @Override
        public void sendRetryMessage(int retryNo) {}

        @Override
        public URI getRequestURI() {
            return URI.create(SP_API_URL + uri);
        }

        @Override
        public Header[] getRequestHeaders() {
            return headers;
        }

        @Override
        public void setRequestURI(URI requestURI) {}

        @Override
        public void setRequestHeaders(Header[] requestHeaders) {
            this.headers = requestHeaders;
        }

        @Override
        public void setUseSynchronousMode(boolean useSynchronousMode) {
            this.sync = useSynchronousMode;
        }

        @Override
        public boolean getUseSynchronousMode() {
            return sync;
        }
    }
}
