package org.smartpaws.net;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.smartpaws.objects.Convention;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.smartpaws.MainActivity.APP_NAME;

public class DataMan {

    public static final Gson GSON = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .setPrettyPrinting().create();

    private static final Map<String, Convention> conventions = new ConcurrentHashMap<>();

    public static void getConvention(final String name, final ResponseHandler<Convention> response) {
        Convention result = conventions.get(name);
        if (result != null) {
            response.result(result);
            return;
        }

        HttpClient.get(name + ".json", null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    Convention convention = GSON.fromJson(new String(responseBody, "UTF-8"), Convention.class);
                    conventions.put(name, convention);
                    response.result(convention);
                } catch (Exception ex) {
                    Log.e(APP_NAME, "", ex);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e(APP_NAME, "HTTP Error: " + statusCode + "\nURL:" + "img/" + name + ".json", error);
            }
        });
    }

    public static void getImage(final String name, final String extension, final ResponseHandler<Bitmap> response) {
        HttpClient.get("img/" + name + extension, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                response.result(BitmapFactory.decodeByteArray(responseBody, 0, responseBody.length));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e(APP_NAME, "HTTP Error: " + statusCode + "\nURL:" + "img/" + name + extension, error);
            }
        });
    }

    public static void getByteArray(final String name, final String extension, final ResponseHandler<byte[]> response) {
        HttpClient.get(name + extension, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                response.result(responseBody);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e(APP_NAME, "HTTP Error: " + statusCode + "\nURL:" + name + extension, error);
            }
        });
    }

    public static void getParallelByteArray(final int index, final String name, final String extension, final ParallelResponseHandler<byte[]> response) {
        HttpClient.get(name + extension, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                response.result(index, responseBody);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e(APP_NAME, "HTTP Error: " + statusCode + "\nURL:" + "img/" + name + extension, error);
            }
        });
    }
}
