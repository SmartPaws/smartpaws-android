package org.smartpaws.net;

public interface ResponseHandler<T> {

    public void result(T result);
}
